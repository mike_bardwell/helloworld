#include "s_expression_parser.hpp"
#include "gtest/gtest.h"

namespace {

TEST(SExpressionParserTest, Strings) {
    EXPECT_EQ("hi", function_for_testing("hi")); // pass
    EXPECT_NE("bye", function_for_testing("hi")); // pass
    // EXPECT_NE("bye", function_for_testing("bye")); // fail
}

}