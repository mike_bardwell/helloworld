#include "s_expression_parser.hpp"

typedef std::map<std::string, std::string> element_data_type_t;
static const element_data_type_t element_data_type =
{
    {"id", "std::string"},
    {"name", "std::string"},
    {"timestamp", "std::string"},
    {"mode", "std::string"},
    {"frequency", "unsigned int"},
    {"fps", "unsigned int"},
    {"exposure", "unsigned int"},
    {"gain_r", "float"},
    {"gain_g", "float"},
    {"gain_b", "float"},
    {"latitude", "double"},
    {"longitude", "double"},
    {"altitude", "double"},
};

bool allowed_char(char ch)
{
    if (ch >= '0') return true;
    return false;
}

void extract_from_line(std::string& line, std::vector<std::string>& csv_vector)
{
    std::string element;
    std::string data_type;
    std::string value;
    bool drop_row;
    int i = 0;

    while (line[i])
    {
        drop_row = false;
        element.clear();
        value.clear();
        while(line[i] && line[i++] != '(');
        if (allowed_char(line[i]))
        {
            while(line[i] && line[i] != ' ') 
            {
                element += line[i];
                i++;
            }
            while(line[i] && line[i] == ' ') i++;
            while(line[i] && line[i] != ')')
            {
                value += line[i];
                i++;
            }

            element_data_type_t::const_iterator pos = element_data_type.find(element);
            if (pos == element_data_type.end()) 
            {
                drop_row = true;
            } 
            else 
            {
                data_type = pos->second;
            }
            if (!drop_row)
            {
                csv_vector.push_back(element + ',' + data_type + ',' + value);
            }
        }
    }
}

std::vector<std::string> *get_element_datatype_value_list(std::ifstream& infile)
{
    std::string line;
    std::vector<std::string> *csv_vector = new std::vector<std::string>;
    while (std::getline(infile, line))
    {
        extract_from_line(line, *csv_vector);
    }
    return csv_vector;
}

std::string function_for_testing(std::string foo)
{
    return foo;
}