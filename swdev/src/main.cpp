#include "s_expression_parser.hpp"

int main()
{
    std::ifstream infile("../example.sexp");
    std::vector<std::string> *csv_vector = get_element_datatype_value_list(infile);
    int n = csv_vector->size();
    for (int i=0; i < n; i++)
    {
        std::cout << csv_vector->at(i) << std::endl;
    }
    return 0;
}