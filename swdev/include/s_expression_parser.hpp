#pragma once

#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <stdbool.h>

std::vector<std::string> *get_element_datatype_value_list(std::ifstream& infile);

std::string function_for_testing(std::string);