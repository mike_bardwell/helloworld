# S-expression parser

## Build

1. Clone repository
2. Run ./setup in /swdev
3. Change to /build dev and use cmake with target defines ```e.g. cmake ..```

## Run, 

For x86, RPi, BB targets: use Makefile (created by CMake) to compile. In the example ```test.cpp```, I import ```s_expression.parser.hpp``` and stream my data into ```get_element_datatype_value_list()```

For embedded targets (potentially in emulator): use make flash to program target. Makefile would have something like ```flash: nrfjprog --prog nrf52``` in it

## Review

* Clone repo https://github.com/GHGSat/tech-challenge.git
* Initial thoughts
  *.sexp file uses spaces and brackets as deliminators (thought maybe tabs as well, but vscode toggle whitespace shows file uses spaces)
  * First thoughts: to parse, essentially string compare for ASCII value 0x28 (open bracket), grab first string as key (aka data-type) and value after space (if exists). If there is no closing bracket right after value do not print 
    * Sent email to Emeric to clarify “element, data-type, value” from requirements. He responded with OK and told me to output in CSV file format
  * Selected C++ for parsing
       * 1/2 should be a simple .cpp file
       * 3 CMake
       * 4 RPi and BB have Linux OS's. Never used a gemu emulator but if it includes microcontroller targets, Print functions should be tied to communication HW [e.g. uart]
  * Write parser in cpp
     * Using VSCode with Code Runner add on for quick testing
     * “/bin/sh: 1: cd: can't cd to extension-output-#5" - [fixed - infile.open required full path]
     * Step 1: Line-by-line input code. If time I could write a class ([somewhat good example](https://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c)) that you can stream the file into. It could provide methods to read data, write data, create map with data...
     * Step 2: extract and print element/data-type/value
        * Extract data type: [repeat] to parse, essentially string compare for ‘(’, grab first string as key (aka element) and value after space (if exists). If there is no closing bracket rigaht after value, pop row
        * To determine data type, I could have a prepared element:data-type map. This would have to be updated as new elements are created though. I could use strtol to automate finding #'s and assume others are strings, but I run into issues like: what if number is not base 10? strtol returns long ints, so it doesn't even give the optimal type (float, uint, int, ...), is it worth it? Going with map method
        * Inconsistencies in .sexp file (multiple opening brackets, multiple spaces between "gain_r  250") made me switch from a for-loop/boolean method to a while-loop/iterator method
     * Step 3: build from CMake. Wrote README
        * I'm out of time, but I would use [this CMake reference](https://cmake.org/cmake/help/v3.9/manual/cmake-toolchains.7.html#cross-compiling-for-linux) to build a cross-compiling toolchain
        * Tried to follow the build process on my BB. Forgot my password and did not have time to upload new Linux image
        
~5 hours of work
        
## Improve

1. For embedded targets, ensure print output goes through communication hardware
2. Write unit tests for functions in s_expression_parser.cpp
3. Learn more about gemu emulators
