FROM debian:stable-slim

LABEL author="Mike"
LABEL version="beta-af"

RUN echo "*** Installing C++ Compilers ***" \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get clean \
    && apt-get update \
    && apt-get dist-upgrade -y
# We could install all of them in a single line, 
# but if we later want to add or remove a package,
# we need to re-run the whole process. 
# So the best practice here is to install one package per line 
# so you can benefit from Docker’s caching
RUN apt-get install -qy build-essential
RUN apt-get install -qy cmake
RUN apt-get install -qy git

RUN apt-get autoremove --purge -y \
    && apt-get autoclean -y \
    && rm -rf /var/cache/apt/* /tmp/*